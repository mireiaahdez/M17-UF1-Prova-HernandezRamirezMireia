using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public GameObject Player;
    public UIScript uIScript;
    private static GameManager instance;

    private float _score;

    public float Score
    {
        get { return _score; }
    }

    public static GameManager Instance
    {
        get { return instance; }

    }


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        _score = 0;

        Player = GameObject.Find("Player");
        uIScript = GameObject.Find("Canvas").GetComponent<UIScript>();
    }

    public void IncreaseScore(float plusScore)
    {
        _score += plusScore;
    }
}
