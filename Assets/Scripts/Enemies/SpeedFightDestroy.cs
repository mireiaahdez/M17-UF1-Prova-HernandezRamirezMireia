using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFightDestroy : MonoBehaviour
{

    private int lives;
    // Start is called before the first frame update
    void Start()
    {
        lives = 2;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("Bullet")){
            lives--;
            if (lives <= 0)
            {
                GameManager.Instance.IncreaseScore(20);
                Destroy(gameObject);
            }
        }
    }
}
