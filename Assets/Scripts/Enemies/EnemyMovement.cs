using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.Animation;

public class EnemyMovement : MonoBehaviour
{

    private SpriteRenderer _spriteRenderer;
    [SerializeField]
    private float speed;
    private Transform _transform;
    
    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();   
        _transform = GetComponent<Transform>();
        speed = 8;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, (Time.deltaTime * speed), 0f);
    }
}
