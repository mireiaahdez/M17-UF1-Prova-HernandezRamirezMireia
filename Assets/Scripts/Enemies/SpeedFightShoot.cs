using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFightShoot : MonoBehaviour
{

    public GameObject _bulletPrefab;
    public Transform _firePoint;
    private float _fireRate;
    // Start is called before the first frame update
    void Start()
    {
        _fireRate = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(_fireRate <= 0)
        {
            BulletShoot();
            _fireRate = 1;

        }
        else
        {
            _fireRate -= Time.deltaTime;
        }
    }

    void BulletShoot()
    {
        Instantiate(_bulletPrefab, _firePoint.position, _firePoint.rotation);
    }
}
