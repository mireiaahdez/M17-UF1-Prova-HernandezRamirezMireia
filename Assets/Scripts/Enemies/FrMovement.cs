using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrMovement : MonoBehaviour
{

    private SpriteRenderer _spriteRenderer;
    [SerializeField]
    private float speed;
    private Transform _transformfr;

    // Start is called before the first frame update
    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _transformfr = GetComponent<Transform>();
        speed = 3;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, (Time.deltaTime * speed), 0f);
    }
}
