using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private SpriteRenderer _spriteRenderer;
    public float speed;
    public Transform _playerTransform;


    
    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        float hz = Input.GetAxisRaw("Horizontal");
        float vt = Input.GetAxisRaw("Vertical");

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(0f, hz * Time.deltaTime * speed, 0f);

        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(0f, hz * Time.deltaTime * speed, 0f);

        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate( -vt * Time.deltaTime * speed, 0f,0f);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(-vt * Time.deltaTime * speed, 0f, 0f);
        }
    }
}
