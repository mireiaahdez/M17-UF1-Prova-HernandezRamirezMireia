using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Transform _firePoint;
    public GameObject _bulletPrefab;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShootBullet();
        } 
    }

    public void ShootBullet()
    {
        Instantiate(_bulletPrefab, _firePoint.position, _firePoint.rotation );
    }
}
