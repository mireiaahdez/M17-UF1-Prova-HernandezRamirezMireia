using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{

    [SerializeField]
    private GameObject[] enemies = new GameObject[2];

    float nextSpawn = 0.0f;
    float spawnRate = 4f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int enemy = Random.Range(0, 2);
        nextSpawn = Time.time + spawnRate;
        spawnRate = Random.Range(1, 6);
        Instantiate(enemies[enemy], transform.position, Quaternion.identity);
        
    }
}
